#ifndef PHYSRECTANGLE_H
#define PHYSRECTANGLE_H
#include <SFML/Graphics/RectangleShape.hpp>
#include <SFML/System/Vector2.hpp>
using namespace sf;

class PhysRectangle : public sf::RectangleShape {
public:
	Vector2f velocity;
	Vector2f acceleration;
	float mass;
public:
	PhysRectangle();
	~PhysRectangle();
};

#endif
