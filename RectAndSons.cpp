#include "RectAndSons.h"

RectAndSons::RectAndSons() {
	cuadradongo.setSize(Vector2f(32.f,32.f));
	cuadradongo.setFillColor(Color::Cyan);
	update();
}

RectAndSons::~RectAndSons() {
	
}
void RectAndSons::update(){
	cuadradongo.setPosition(RectAndSons::getPosition());
}
void RectAndSons::draw(sf::RenderTarget& target, sf::RenderStates states) const {
	target.draw(cuadradongo, states);
}
