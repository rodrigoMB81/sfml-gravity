#ifndef GAME_H
#define GAME_H
#include <SFML/Graphics.hpp>
#include <vector>
#include "PhysRectangle.h"
#include "PhysCircle.h"
#include <cmath>
#include <cstdlib>
//for debugging purposes only
#include <iostream>
//wololo

using namespace sf;

class Game {
private:
	void processEvents();
	void update(Time);
	void render();
	void crearPhysCircle();
	void destruirPhysCircle();
	bool hasCrashed(PhysCircle&, PhysCircle&);
	void applyGravity(PhysCircle&, PhysCircle&, Time);
	
private: //objetos
	RenderWindow mWindow;
	std::vector<PhysCircle>vectorDeCirculos;
	std::vector<PhysCircle>::iterator iteradorA;
	std::vector<PhysCircle>::iterator iteradorB;
	std::vector<PhysCircle>::iterator destroyCircle;
	Clock cronometro;
	VertexArray accelVector;
	VertexArray velocityVector;
	Vector2f newVelocity;
	Vector2f newAcceleration;
	float newMass;
	Vector2f position;
	Vector2f linearMomentum;
	Vector2f newPosition;

private: //variables
	Time elapsedTime;
	Time tiempoPorCuadro;
	Time otroTiempo;
	float G;
	bool showVectors;
	bool activarChoque;
	bool createNew;
	
public:
	void run();
	void interpretarEntrada(sf::Keyboard::Key, bool);
	float getLength(Vector2f);
	float dotProduct(Vector2f, Vector2f);
	Vector2f unitVector(Vector2f);
	float calculateRadius(float);
	float base;
	float masaMinima;
	Game();
	~Game();
};

#endif

