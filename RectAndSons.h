#ifndef RECTANDSONS_H
#define RECTANDSONS_H
#include <SFML/Graphics.hpp>
//#include <SFML/Graphics/Drawable.hpp>
//#include <SFML/Graphics/RectangleShape.hpp>
//#include <SFML/System/Vector2.hpp>
//#include <SFML/Graphics/VertexArray.hpp>

using namespace sf;
class RectAndSons : public sf::RectangleShape{//, public sf::Drawable {
private:
	virtual void draw(sf::RenderTarget& target, sf::RenderStates states) const;
	RectangleShape cuadradongo;
protected:
public:
	RectAndSons();
	~RectAndSons();
	void update();
};

#endif

