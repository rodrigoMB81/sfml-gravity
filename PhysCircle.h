#ifndef PHYSCIRCLE_H
#define PHYSCIRCLE_H
#include <SFML/Graphics/CircleShape.hpp>
#include <SFML/System/Vector2.hpp>
using namespace sf;
class PhysCircle : public CircleShape {
public:
	Vector2f velocity;
	Vector2f acceleration;
	float mass;
public:	
	PhysCircle();
	~PhysCircle();
};

#endif

