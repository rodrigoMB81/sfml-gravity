#include "Game.h"
Game::Game() {
	mWindow.create(VideoMode(800,600), "Titulo");
	tiempoPorCuadro = sf::seconds(1.f/60.f);
	G = 6.f*pow(10.f,-5.f);//2.f
	srand(time(NULL));
	//todo lo relacionado con los representantes graficos de los vectores accel y vel
	accelVector.setPrimitiveType(sf::Lines);
	accelVector.resize(2);
	velocityVector.setPrimitiveType(sf::LinesStrip);
	velocityVector.resize(2);
	accelVector[0].color = Color::Green;
	accelVector[1].color = Color::Green;
	velocityVector[0].color = Color::Red;
	velocityVector[1].color = Color::Red;
	showVectors = false;
	activarChoque = false;
	iteradorA = vectorDeCirculos.begin();
	iteradorB = vectorDeCirculos.begin();
	createNew = false;
	masaMinima = 280.f;
	base = log(25.f);
}

Game::~Game() {
	
}

void Game::run(){
	while(mWindow.isOpen()){
		otroTiempo = cronometro.restart();
		elapsedTime += otroTiempo;
		if(elapsedTime>=tiempoPorCuadro){
			processEvents();
			update(tiempoPorCuadro);
			render();
			elapsedTime=Time::Zero;
		}
	}
}
void Game::processEvents(){
	Event eventos;
	while(mWindow.pollEvent(eventos)){
		switch(eventos.type){
			case(Event::Closed):
				mWindow.close();
				break;
			case(Event::KeyPressed):
				interpretarEntrada(eventos.key.code, true);
				break;
			case(Event::KeyReleased):
				interpretarEntrada(eventos.key.code, false);
				break;
		}
	}
}
void Game::update(Time sinceLastFrame){

	if(vectorDeCirculos.size()>=1){
		for(iteradorA = vectorDeCirculos.begin(); iteradorA!=vectorDeCirculos.end(); iteradorA++){
			for(iteradorB = vectorDeCirculos.begin(); iteradorB!=vectorDeCirculos.end(); iteradorB++){
				if(iteradorA == iteradorB){
					continue;
				}
				
				if(hasCrashed(*iteradorA, *iteradorB) == true && activarChoque == true){
					createNew=true;
					linearMomentum.x = iteradorA->mass * iteradorA->velocity.x + iteradorB->mass * iteradorB->velocity.x;
					linearMomentum.y = iteradorA->mass * iteradorA->velocity.y + iteradorB->mass * iteradorB->velocity.y;
					newMass = iteradorA->mass + iteradorB->mass;
					newVelocity.x = linearMomentum.x / newMass;
					newVelocity.y = linearMomentum.y / newMass;
					Vector2f newAcceleration(0.f,0.f); //esto deberia ser 0,0 ??
					
					if(iteradorA->getRadius()>=iteradorB->getRadius()){
						newPosition=iteradorA->getPosition();
						iteradorA->setRadius(calculateRadius(newMass));
						iteradorA->mass = newMass;
						iteradorA->velocity = newVelocity;
						iteradorA->acceleration = newAcceleration;
						destroyCircle = iteradorB;
					}
					else{
						newPosition=iteradorB->getPosition();
						iteradorB->setRadius(calculateRadius(newMass));
						iteradorB->mass = newMass;
						iteradorB->velocity = newVelocity;
						iteradorB->acceleration = newAcceleration;
						destroyCircle = iteradorA;
					}
				}
				else{
					applyGravity(*iteradorA, *iteradorB, tiempoPorCuadro);
				}
			}
		}
	}
	if(createNew == true){
		createNew=false;
		vectorDeCirculos.erase(destroyCircle);
		
	}
}
void Game::render(){
	mWindow.clear(Color::Black);
	for(iteradorA=vectorDeCirculos.begin(); iteradorA!=vectorDeCirculos.end(); iteradorA++){
		mWindow.draw(*iteradorA);
		if(showVectors == true){
			float radius = iteradorA->getRadius();
			Vector2f posicion = iteradorA->getPosition();
			Vector2f aceleracion = iteradorA->acceleration;
			Vector2f velocidad = iteradorA->velocity;
			accelVector[0].position = posicion;
			accelVector[1].position = accelVector[0].position + aceleracion*1000.f;
			velocityVector[0].position = posicion;
			velocityVector[1].position = velocityVector[0].position + velocidad*2.f;
			mWindow.draw(accelVector);
			mWindow.draw(velocityVector);
		}
	}
	mWindow.display();
}
void Game::interpretarEntrada(Keyboard::Key tecla, bool isPressed){
	switch(tecla){
		case(Keyboard::F):
			activarChoque = true;
			std::cout<<std::endl<<"activarChoque:"<<activarChoque;
			break;
		case(Keyboard::G):
			activarChoque = false;
			std::cout<<std::endl<<"activarChoque:"<<activarChoque;
			break;
		case(Keyboard::A):
			crearPhysCircle();
			break;
		case(Keyboard::S):
			destruirPhysCircle();
			break;
		case(Keyboard::Z):
			showVectors=true;
			std::cout<<std::endl<<"showVectors:"<<showVectors;
			break;
		case(Keyboard::X):
			showVectors=false;
			std::cout<<std::endl<<"showVectors:"<<showVectors;
			break;
	}
}
void Game::crearPhysCircle(){
	PhysCircle * circuloTemporal;
	circuloTemporal = new PhysCircle();
	circuloTemporal->mass = 280 + rand()%41;
	circuloTemporal->setRadius(calculateRadius(circuloTemporal->mass));
	float radius = circuloTemporal->getRadius();
	circuloTemporal->setOrigin(Vector2f(radius, radius));
	circuloTemporal->setFillColor(Color(rand()%256,rand()%256,rand()%256));
	circuloTemporal->setPosition(Vector2f((rand()%801),(rand()%601)));
	vectorDeCirculos.push_back(*circuloTemporal);
	delete circuloTemporal;
}
void Game::destruirPhysCircle(){
	if(vectorDeCirculos.size()>0)
		vectorDeCirculos.pop_back();
	std::cout<<std::endl<<vectorDeCirculos.size();
}
float Game::getLength(Vector2f vectorUno){
	return sqrt( (vectorUno.x * vectorUno.x) + (vectorUno.y * vectorUno.y) );
}
float Game::dotProduct(Vector2f vectorUno, Vector2f vectorDos){
	float resultado = (vectorUno.x*vectorDos.x) + (vectorDos.y*vectorDos.y);
	return resultado;
}
Vector2f Game::unitVector(Vector2f vectorUno){
	Vector2f resultado = vectorUno;
	resultado.x = resultado.x / getLength(resultado);
	resultado.y = resultado.y / getLength(resultado);
	return resultado;
}
bool Game::hasCrashed(PhysCircle& circuloA, PhysCircle& circuloB){
	
	Vector2f positionA = circuloA.getPosition();
	Vector2f positionB = circuloB.getPosition();
	float radioDeChoque;
	
	if(circuloA.getRadius()>circuloB.getRadius())
		radioDeChoque = circuloA.getRadius();
	else
		radioDeChoque = circuloB.getRadius();
	
	if(abs(getLength(positionA-positionB))<radioDeChoque)
		return true;
	else
		return false;
}
void Game::applyGravity(PhysCircle& particulaA, PhysCircle& particulaB, Time tiempo){
	Vector2f distancia = particulaA.getPosition() - particulaB.getPosition();
	float fuerza = (G*particulaA.mass*particulaB.mass) / pow(getLength(distancia),2.f);
	float aceleracionA = fuerza / particulaA.mass;
	float aceleracionB = fuerza / particulaB.mass;
	Vector2f director = unitVector(distancia);
	particulaA.acceleration = (director * (-1.f)) * aceleracionA;
	particulaB.acceleration = director * aceleracionB;
	
	particulaA.velocity += particulaA.acceleration;
	particulaB.velocity += particulaB.acceleration;
	
	particulaA.setPosition(particulaA.getPosition() + particulaA.velocity * tiempo.asSeconds());
	particulaB.setPosition(particulaB.getPosition() + particulaB.velocity * tiempo.asSeconds());
}
float Game::calculateRadius(float mass){
	return (log(mass) / base) + 1.f;
}
